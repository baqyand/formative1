import java.util.Date;

public class Cleaning extends Employee{
    //inisialisasi data clea
    public String[] cleaningGears;

    public String[] getCleaningGears() {
        return cleaningGears;
    }

    public void setCleaningGears(String[] cleaningGears) {
        this.cleaningGears = cleaningGears;
    }

    public Cleaning(String[] cleaningGears){
        super();
        this.cleaningGears = cleaningGears;
    }
    public Cleaning(String name, Date startDate, int salary, int startWorkingHour, int endWorkingHour, String uniform){
        super(name, startDate, salary, startWorkingHour, endWorkingHour, uniform);
    }
}
