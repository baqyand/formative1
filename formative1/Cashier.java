public class Cashier extends Employee{
    private int hasilPenjualanTicket;
    private int salary;

    public Cashier(int hasilPenjualanTicket){
        super();
        this.hasilPenjualanTicket = hasilPenjualanTicket;
    }

    @Override
    public int getSalary() {
        return salary;
    }

    @Override
    public void setSalary(int salary) {
        this.salary = salary;
    }
}
