import java.util.Date;

public class BioskopApp {

    public static void main(String[] args) {
        //inisialisasi object
        Bioskop b1 = new Bioskop("XII TSM","Bandung","08.00");
        Studio s1 = new Studio("Studio 1",60);
        Movie m1 = new Movie("perempuan anak mamahnya",25000);
        Date date = new Date();

        //menampilkan data bioskop
        System.out.println("======= Bioskop =======");
        System.out.println("Nama Bioskop : " + b1.name);
        System.out.println("Alamatnya : " + b1.addres);

        //set object, set data employee via getter and setter
        Cashier cashier = new Cashier(60);
        cashier.setName("Bama");
        cashier.setStartDate(date);
        cashier.setSalary(4500000);
        cashier.setStartWorkingHour(07);
        cashier.setEndWorkingHour(16);
        cashier.setUniform("hitam");

        //set object cleaning via constructtor dan set gears
        Cleaning clean = new Cleaning("Qyan",date,3500000,05,20,"Kuning");
        String[] cleaningGears = {"Sapu","Penyedot Debu"};
        clean.setCleaningGears(cleaningGears);

        //set object dan gears security
        Security security = new Security("Dija",date,4000000,12,5,"PDL");
        String[] securityGears = {"Tazer","Bat","Walkie Talkie","Ikat"};
        String[] certification = {"Security trained certification", "Health certification","Vaccine"};
        security.setSecurityGears(securityGears);
        security.setCertification(certification);


        //output data para employee
        //cashier
        System.out.println("\n============ EMPLOYEE ============");
        System.out.println("Cashier");
        System.out.println("Nama : " + cashier.getName());
        System.out.println("Awal masuk : " + cashier.getStartDate());
        System.out.println("gaji : " + cashier.getSalary());
        System.out.println("jam masuk kerja : " + cashier.getStartWorkingHour() + " AM");
        System.out.println("jam pulang kerja : " + cashier.getEndWorkingHour() + " PM");
        System.out.println("Uniform : " + cashier.getUniform());

        //security
        System.out.println("\nSecurity");
        System.out.println("Nama : " + security.getName());
        System.out.println("Awal masuk : " + security.getStartDate());
        System.out.println("gaji : " + security.getSalary());
        System.out.println("jam masuk kerja : " + security.getStartWorkingHour() + " AM");
        System.out.println("jam pulang kerja : " + security.getEndWorkingHour() + " PM");
        System.out.println("Uniform : " + security.getUniform());
        System.out.println("gear : " );
        for (var data : securityGears){
            System.out.print(data + ", ");
        }
        System.out.println("Certificate : ");
        for (var data : certification){
            System.out.print(data + ", ");
        }

        //cleaning service
        System.out.println();
        System.out.println("\nCleaning Service");
        System.out.println("Nama : " + clean.getName());
        System.out.println("Awal masuk : " + clean.getStartDate());
        System.out.println("gaji : " + clean.getSalary());
        System.out.println("jam masuk kerja : " + clean.getStartWorkingHour() + " AM");
        System.out.println("jam pulang kerja : " + clean.getEndWorkingHour() + " PM");
        System.out.println("Uniform : " + clean.getUniform());
        System.out.println("gear : " );
        for (var data : cleaningGears){
            System.out.print(data + ", ");
        }


        //output dari Studio
        System.out.println("\n\n========= Studio =========");
        System.out.println("nama Studio : " + s1.name);
        System.out.println("seat : " + s1.seatsAmount);


        //sout dari movie
        System.out.println("\n========= Movie =========");
        System.out.println("Nama Movie : " + m1.getTitle());
        System.out.println("Harga : " + m1.getPrice());

    }
}
