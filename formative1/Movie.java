public class Movie extends Bioskop{
    private int price;
    private String title;

    public int getPrice() {
        return price;
    }

    public String getTitle() {
        return title;
    }

    public Movie(String title, int price){
        super();
        this.price = price;
        this.title = title;
    }

}
