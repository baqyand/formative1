public class Studio extends Bioskop{
    public String name;
    public int seatsAmount;

    public Studio(String name, int seatsAmount){
        super();
        this.name = name;
        this.seatsAmount = seatsAmount;
    }
}
