import java.util.Date;

public class Employee extends Bioskop{
    private String name;
    private Date startDate;
    private int salary;
    private int startWorkingHour;
    private int endWorkingHour;
    private String uniform;

    public Employee() {

    }
    public Employee(String name, Date startDate, int salary, int startWorkingHour, int endWorkingHour, String uniform){
        this.name = name;
        this.startDate = startDate;
        this.salary = salary;
        this.startWorkingHour = startWorkingHour;
        this.endWorkingHour = endWorkingHour;
        this.uniform = uniform;

    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public int getSalary() {
        return salary;
    }

    public void setSalary(int salary) {
        salary = salary;
    }

    public int getStartWorkingHour() {
        return startWorkingHour;
    }

    public void setStartWorkingHour(int startWorkingHour) {
        this.startWorkingHour = startWorkingHour;
    }

    public int getEndWorkingHour() {
        return endWorkingHour;
    }

    public void setEndWorkingHour(int endWorkingHour) {
        this.endWorkingHour = endWorkingHour;
    }

    public String getUniform() {
        return uniform;
    }

    public void setUniform(String uniform) {
        this.uniform = uniform;
    }



}

