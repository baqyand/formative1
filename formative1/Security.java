import java.util.Date;

public class Security extends Employee {
    private String[] certification;
    private String[] securityGears;

    public Security(String name, Date startDate, int salary, int startWorking, int endWorking, String uniform) {
        super(name, startDate, salary, startWorking, endWorking, uniform);
    }

    public String[] getCertification() {
        return certification;
    }

    public String[] getSecurityGears() {
        return securityGears;
    }

    public void setCertification(String[] certification) {
        this.certification = certification;
    }

    public void setSecurityGears(String[] securityGears) {
        this.securityGears = securityGears;
    }
}
